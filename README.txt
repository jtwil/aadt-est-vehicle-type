===== AADT Estimation by Vehicle Type =====

This public repository hosts the project artefacts arising from the paper
"Estimating Annual Average Daily Traffic by Vehicle Type using Machine Learning:
A Case Study in the North East of England" by Jordan Wilson, Newcastle
University.

The following file tree describes the contents of this repository. The most
important files are `ne-traffic.qgz` (the GIS environment file),
`pyqgis-code.txt` (snippets of PyQGIS code used to annotate the count points
with predictor variables) and the `colab` directory (the notebooks used for
clustering and Machine Learning).


│   pyqgis-code.txt
│	Snippets of PyQGIS code to perform annotation of predictor variables to the
│       count points
├───colab	Google Colab (Jupyter) notebooks
│       CLUSTERING.ipynb
│		Code to perform the clustering	
│       VTAADT.ipynb
│		Code to train and test the Machine Learning models
│
├───data	Datasets containing attributes to annotate to the AADT count points
│   │   AGE_DAYPOP_ECOACT_LSOADZ_England_Northern_Ireland_Wales_Descriptions.csv
│   │		LSOA Workplace population (source dataset)
│   │   incomeestimatesforsmallareasdatasetfinancialyearending20181.xls
│   │		MSOA household income (source dataset)
│   │   localincomedeprivationdata.xlsx
│   │		LSOA income deprivation score
│   │   lsoa-deprivation-income-scores.csv
│   │		LSOA Index of Multiple Deprivation score
│   │   lsoa-households.csv
│   │		LSOA Number of households by type (wrangled dataset)
│   │   lsoa-population.csv
│   │		LSOA population	by age group (wrangled dataset)
│   │   lsoa-population_density.csv
│   │		LSOA population	density (wrangled dataset)
│   │   lsoa-to_msoa.csv
│   │		Mapping of LSOA to MSOA (wrangled dataset)
│   │   lsoa-vehicles-registered.csv
│   │		LSOA Number of registered vehicles (wrangled dataset)
│   │   lsoa-workplace-population.csv
│   │		LSOA Workplace population (wrangled dataset)
│   │   msoa-income.csv
│   │		MSOA household income (wrangled dataset)
│   │   National_Statistics_Postcode_Lookup_UK_Coordinates.csv
│   │		Postcode to geo-coordinate mapping
│   │   ne-msoa-popden.csv
│   │		MSOA population density
│   │   PCD_OA_LSOA_MSOA_LAD_FEB19_UK_LU.csv
│   │		Mapping of LSOA to MSOA (source dataset)
│   │   RAW-lsoa-households.csv
│   │		LSOA Number of households by type (source dataset)
│   │   RAW-lsoa-registered-vehicles.csv
│   │		LSOA Number of households by type (source dataset)
│   │   sape23dt11mid2020lsoapopulationdensity.xlsx
│   │		LSOA population	density (source dataset)
│   │   sape23dt13mid2020lsoabroadagesestimatesunformatted.xlsx
│   │		LSOA population by age group (source dataset)
│   │   uk-englandwales-ndr-2023-listentries-compiled-epoch-0002-baseline-csv.csv
│   │		VOA land-use characteristics (source dataset)
│   │   voa_scat.csv
│   │		List of SCat code in North East
│   │   voa_scat_desc.csv
│   │		List of SCat codes, with description and project-assigned summary category
│   │   voa_scat_final.csv
│   │		Land-use points, with summary cat.s labelled (used in the GIS environment)
│   │   voa_scat_key.csv
│   │		Key for summary categories
│   │
│   ├───clustering	Clustering related files
│   │       3_buf_for_clustering.csv
│   │		Data used for clustering
│   │	    all_hgvs_labelled.csv
│   │		HGV clusters    
│   │	    buses_and_coaches_labelled.csv
│   │		Bus clusters    
│   │	    cars_and_taxis_labelled.csv
│   │		Car clusters
│   │	    cluster-classes.ods
│   │		Tables summarising clustering results
│   │	    lgvs_labelled.csv
│   │		LGV clusters
│   │	    two_wheeled_motor_vehicles_labelled.csv
│   │		Motorcycle clusters
│   │	 
│   ├───final-buffers	Count points with distance-based variables for each radius ("buffer") 
│   │       1600m.csv
│   │		1600m radius
│   │       3200m.csv
│   │		3200m radius
│   │       3_buf_data.csv
│   │		Count points annotated all three radii results
│   │       500m.csv
│   │		500m radius
│   │
│   ├───ml-datasets	Datasets used for ML (format: radius_vehicle_nonnorm.csv)
│   │       m1600_bus_nonnorm.csv
│   │	    m1600_cars_nonnorm.csv
│   │	    m1600_hgv_nonnorm.csv
│   │	    m1600_lgvs_nonnorm.csv
│   │	    m1600_two_whe_nonnorm.csv
│   │	    m3200_bus_nonnorm.csv
│   │	    m3200_cars_nonnorm.csv
│   │	    m3200_hgv_nonnorm.csv
│   │	    m3200_lgvs_nonnorm.csv
│   │	    m3200_two_whe_nonnorm.csv
│   │	    m500_bus_nonnorm.csv
│   │	    m500_cars_nonnorm.csv
│   │	    m500_hgv_nonnorm.csv
│   │	    m500_lgvs_nonnorm.csv
│   │	    m500_two_whe_nonnorm.csv
│   │
│   └───results		Results of Machine Learning
│           gbr-best.csv
│		Best radius results for each count point with GBR
│           gbr.csv
│		Results for GBR	    
│           mars-best.csv
│		Best radius results for each count point with MARS
│           mars.csv
│		Results for GBR	    
│           results-table.ods
│           svm.csv
│		Results for SVR
│           svr-best.csv
│		Best radius results for each count point with SVR
│   
└───maps	Spatial datasets
    │   buffers.gpkg
    │		Radii from each count point
    │   buffersFilled.gpkg
    │		Radii from each count point, filled with the distance-based vawriables
    │   dft-ne-aadt-2021.gpkg
    │		AADT count points (source for above)
    │   dft-ne-aadt.csv
    │		AADT values (source for above)
    │   dft-ne-raw-traffic-counts.csv
    │		Raw traffic counts (not used)
    │   motorway-junctions.gpkg
    │		Points of motorway junctions    	
    │   ne-built-up-areas-centroids.gpkg
    │		Centroids of the North East's built-up areas    
    │   ne-built-up-areas.gpkg
    │		Polygons of the North East's built-up areas
    │   ne-charge-points.gpkg
    │		Locations of the North East's electical vehicle charge-points
    │   ne-eng-dissolved.cpg
    │   ne-eng-dissolved.dbf
    │   ne-eng-dissolved.prj
    │   ne-eng-dissolved.shp
    │   ne-eng-dissolved.shx
    │		A polygon of the North East's shape, with no local authority boundaries
    │   ne-land-boundary.cpg
    │   ne-land-boundary.dbf
    │   ne-land-boundary.prj
    │   ne-land-boundary.shp
    │   ne-land-boundary.shx
    │		A polygon with the North East's land boundary
    │   ne-major-town-centroids.gpkg
    │		Centroids for the North East's eight major towns
    │   ne-major-towns.gpkg
    │		Polygons for the North East's eight major towns
    │   ne-msoa.gpkg
    │		Polygons with North East's MSOA
    │   ne-roads.gpkg
    │		Road links in North East
    │   ne-scat.gpkg
    │		Land-use points (derived from VOA data)
    │   ne-traffic.qgz
    │		Project's QGIS environment	
    │   roadlink.qml
    │		Styling used for the road-link layers
    │
    ├───ne-eng	Polygons of the North East's local authorities
    │       ne-eng.cpg
    │       ne-eng.dbf
    │       ne-eng.prj
    │       ne-eng.qmd
    │       ne-eng.shp
    │       ne-eng.shx
    │
    ├───ne-lsoa		Polygons of the North East's LSOAs
    │       ne-lsoa.cpg
    │       ne-lsoa.dbf
    │       ne-lsoa.prj
    │       ne-lsoa.qmd
    │       ne-lsoa.shp
    │       ne-lsoa.shx
    │
    ├───ne-toll-road-indicators		Location of North East toll roads
    │       ne-toll-road-indicators.cpg
    │       ne-toll-road-indicators.dbf
    │       ne-toll-road-indicators.prj
    │       ne-toll-road-indicators.shp
    │       ne-toll-road-indicators.shx
    │
    └───transport-stops		Locations of transport points
            bus-stations.csv
		Source dataset for bus.gpkg
            bus.gpkg
		Locations of bus stops and stations (separate layers)
            multiplestops.csv
	    	Source dataset for bus.gpkg
            on-street-bus-stops.csv
	    	Source dataset for bus.gpkg
            ports.gpkg
		Locations of North East's airports and sea ports (separate layers) 
            railway-stations.csv
		Source dataset for railway-stations.gpkg
            railway-stations.gpkg
		Locations of North East's railway and Metro stations
