<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis minScale="100000000" simplifyLocal="1" simplifyDrawingTol="1" simplifyAlgorithm="0" hasScaleBasedVisibilityFlag="0" labelsEnabled="1" simplifyDrawingHints="1" readOnly="0" styleCategories="AllStyleCategories" simplifyMaxScale="1" symbologyReferenceScale="-1" version="3.30.1-'s-Hertogenbosch" maxScale="0">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
    <Private>0</Private>
  </flags>
  <temporal endField="" fixedDuration="0" endExpression="" enabled="0" mode="0" durationField="" durationUnit="min" limitMode="0" startExpression="" accumulate="0" startField="">
    <fixedRange>
      <start></start>
      <end></end>
    </fixedRange>
  </temporal>
  <elevation zoffset="0" zscale="1" binding="Centroid" extrusion="0" symbology="Line" type="IndividualFeatures" showMarkerSymbolInSurfacePlots="0" extrusionEnabled="0" clamping="Relative" respectLayerSymbol="1">
    <data-defined-properties>
      <Option type="Map">
        <Option type="QString" name="name" value=""/>
        <Option name="properties"/>
        <Option type="QString" name="type" value="collection"/>
      </Option>
    </data-defined-properties>
    <profileLineSymbol>
      <symbol type="line" frame_rate="10" clip_to_extent="1" force_rhr="0" alpha="1" is_animated="0" name="">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" name="name" value=""/>
            <Option name="properties"/>
            <Option type="QString" name="type" value="collection"/>
          </Option>
        </data_defined_properties>
        <layer class="SimpleLine" enabled="1" locked="0" pass="0" id="{fdd7a8ec-c05a-4392-9869-393780047b3e}">
          <Option type="Map">
            <Option type="QString" name="align_dash_pattern" value="0"/>
            <Option type="QString" name="capstyle" value="square"/>
            <Option type="QString" name="customdash" value="5;2"/>
            <Option type="QString" name="customdash_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="customdash_unit" value="MM"/>
            <Option type="QString" name="dash_pattern_offset" value="0"/>
            <Option type="QString" name="dash_pattern_offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="dash_pattern_offset_unit" value="MM"/>
            <Option type="QString" name="draw_inside_polygon" value="0"/>
            <Option type="QString" name="joinstyle" value="bevel"/>
            <Option type="QString" name="line_color" value="243,166,178,255"/>
            <Option type="QString" name="line_style" value="solid"/>
            <Option type="QString" name="line_width" value="0.6"/>
            <Option type="QString" name="line_width_unit" value="MM"/>
            <Option type="QString" name="offset" value="0"/>
            <Option type="QString" name="offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="offset_unit" value="MM"/>
            <Option type="QString" name="ring_filter" value="0"/>
            <Option type="QString" name="trim_distance_end" value="0"/>
            <Option type="QString" name="trim_distance_end_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="trim_distance_end_unit" value="MM"/>
            <Option type="QString" name="trim_distance_start" value="0"/>
            <Option type="QString" name="trim_distance_start_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="trim_distance_start_unit" value="MM"/>
            <Option type="QString" name="tweak_dash_pattern_on_corners" value="0"/>
            <Option type="QString" name="use_custom_dash" value="0"/>
            <Option type="QString" name="width_map_unit_scale" value="3x:0,0,0,0,0,0"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option name="properties"/>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </profileLineSymbol>
    <profileFillSymbol>
      <symbol type="fill" frame_rate="10" clip_to_extent="1" force_rhr="0" alpha="1" is_animated="0" name="">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" name="name" value=""/>
            <Option name="properties"/>
            <Option type="QString" name="type" value="collection"/>
          </Option>
        </data_defined_properties>
        <layer class="SimpleFill" enabled="1" locked="0" pass="0" id="{bb1af6eb-fe66-4498-a5ec-bf5a38e75e14}">
          <Option type="Map">
            <Option type="QString" name="border_width_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="color" value="243,166,178,255"/>
            <Option type="QString" name="joinstyle" value="bevel"/>
            <Option type="QString" name="offset" value="0,0"/>
            <Option type="QString" name="offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="offset_unit" value="MM"/>
            <Option type="QString" name="outline_color" value="174,119,127,255"/>
            <Option type="QString" name="outline_style" value="solid"/>
            <Option type="QString" name="outline_width" value="0.2"/>
            <Option type="QString" name="outline_width_unit" value="MM"/>
            <Option type="QString" name="style" value="solid"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option name="properties"/>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </profileFillSymbol>
    <profileMarkerSymbol>
      <symbol type="marker" frame_rate="10" clip_to_extent="1" force_rhr="0" alpha="1" is_animated="0" name="">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" name="name" value=""/>
            <Option name="properties"/>
            <Option type="QString" name="type" value="collection"/>
          </Option>
        </data_defined_properties>
        <layer class="SimpleMarker" enabled="1" locked="0" pass="0" id="{e9660773-c3cf-48bc-b80b-64d79bd7fc4d}">
          <Option type="Map">
            <Option type="QString" name="angle" value="0"/>
            <Option type="QString" name="cap_style" value="square"/>
            <Option type="QString" name="color" value="243,166,178,255"/>
            <Option type="QString" name="horizontal_anchor_point" value="1"/>
            <Option type="QString" name="joinstyle" value="bevel"/>
            <Option type="QString" name="name" value="diamond"/>
            <Option type="QString" name="offset" value="0,0"/>
            <Option type="QString" name="offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="offset_unit" value="MM"/>
            <Option type="QString" name="outline_color" value="174,119,127,255"/>
            <Option type="QString" name="outline_style" value="solid"/>
            <Option type="QString" name="outline_width" value="0.2"/>
            <Option type="QString" name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="outline_width_unit" value="MM"/>
            <Option type="QString" name="scale_method" value="diameter"/>
            <Option type="QString" name="size" value="3"/>
            <Option type="QString" name="size_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="size_unit" value="MM"/>
            <Option type="QString" name="vertical_anchor_point" value="1"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option name="properties"/>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </profileMarkerSymbol>
  </elevation>
  <renderer-v2 referencescale="-1" type="RuleRenderer" forceraster="0" symbollevels="1" enableorderby="0">
    <rules key="{752c0a2e-9b6a-4fab-8469-d103fc5e724f}">
      <rule symbol="0" filter="&quot;roadClassification&quot; = 'Motorway'" label="Motorway" key="{92c41ca1-1051-4848-9948-e9bbcfb56ef4}"/>
      <rule symbol="1" filter="&quot;roadClassification&quot; = 'A Road'" label="A Road" key="{fe21e167-412d-448d-b6d6-16fd88ef55ed}"/>
      <rule symbol="2" filter="&quot;roadClassification&quot; = 'B Road'" label="B Road" key="{9094991c-9010-4bd2-a43a-9d064a9fcdd2}"/>
      <rule symbol="3" filter="&quot;roadClassification&quot; = 'Classified Unnumbered'" label="Classified Unnumbered" key="{75808cec-6a15-49ef-af8f-ebaee19bdb75}"/>
      <rule symbol="4" filter="&quot;roadClassification&quot; = 'Unclassified' OR &quot;roadClassification&quot; = 'Not Classified' OR &quot;roadClassification&quot; = 'Unknown'" label="Unclassified" key="{bf98043b-8a86-41e1-a266-ffe660ace031}"/>
    </rules>
    <symbols>
      <symbol type="line" frame_rate="10" clip_to_extent="1" force_rhr="0" alpha="1" is_animated="0" name="0">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" name="name" value=""/>
            <Option name="properties"/>
            <Option type="QString" name="type" value="collection"/>
          </Option>
        </data_defined_properties>
        <layer class="SimpleLine" enabled="1" locked="0" pass="5" id="{8ce130ee-650d-4ca6-8c00-f73330fb2ab6}">
          <Option type="Map">
            <Option type="QString" name="align_dash_pattern" value="0"/>
            <Option type="QString" name="capstyle" value="round"/>
            <Option type="QString" name="customdash" value="5;2"/>
            <Option type="QString" name="customdash_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="customdash_unit" value="MM"/>
            <Option type="QString" name="dash_pattern_offset" value="0"/>
            <Option type="QString" name="dash_pattern_offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="dash_pattern_offset_unit" value="MM"/>
            <Option type="QString" name="draw_inside_polygon" value="0"/>
            <Option type="QString" name="joinstyle" value="bevel"/>
            <Option type="QString" name="line_color" value="0,159,184,255"/>
            <Option type="QString" name="line_style" value="solid"/>
            <Option type="QString" name="line_width" value="1.15"/>
            <Option type="QString" name="line_width_unit" value="MM"/>
            <Option type="QString" name="offset" value="0"/>
            <Option type="QString" name="offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="offset_unit" value="MM"/>
            <Option type="QString" name="ring_filter" value="0"/>
            <Option type="QString" name="trim_distance_end" value="0"/>
            <Option type="QString" name="trim_distance_end_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="trim_distance_end_unit" value="MM"/>
            <Option type="QString" name="trim_distance_start" value="0"/>
            <Option type="QString" name="trim_distance_start_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="trim_distance_start_unit" value="MM"/>
            <Option type="QString" name="tweak_dash_pattern_on_corners" value="0"/>
            <Option type="QString" name="use_custom_dash" value="0"/>
            <Option type="QString" name="width_map_unit_scale" value="3x:0,0,0,0,0,0"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option name="properties"/>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol type="line" frame_rate="10" clip_to_extent="1" force_rhr="0" alpha="1" is_animated="0" name="1">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" name="name" value=""/>
            <Option name="properties"/>
            <Option type="QString" name="type" value="collection"/>
          </Option>
        </data_defined_properties>
        <layer class="SimpleLine" enabled="1" locked="0" pass="4" id="{f556190b-365a-4e56-8ecb-27e36fe53ae1}">
          <Option type="Map">
            <Option type="QString" name="align_dash_pattern" value="0"/>
            <Option type="QString" name="capstyle" value="round"/>
            <Option type="QString" name="customdash" value="5;2"/>
            <Option type="QString" name="customdash_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="customdash_unit" value="MM"/>
            <Option type="QString" name="dash_pattern_offset" value="0"/>
            <Option type="QString" name="dash_pattern_offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="dash_pattern_offset_unit" value="MM"/>
            <Option type="QString" name="draw_inside_polygon" value="0"/>
            <Option type="QString" name="joinstyle" value="bevel"/>
            <Option type="QString" name="line_color" value="212,28,64,255"/>
            <Option type="QString" name="line_style" value="solid"/>
            <Option type="QString" name="line_width" value="1.1"/>
            <Option type="QString" name="line_width_unit" value="MM"/>
            <Option type="QString" name="offset" value="0"/>
            <Option type="QString" name="offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="offset_unit" value="MM"/>
            <Option type="QString" name="ring_filter" value="0"/>
            <Option type="QString" name="trim_distance_end" value="0"/>
            <Option type="QString" name="trim_distance_end_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="trim_distance_end_unit" value="MM"/>
            <Option type="QString" name="trim_distance_start" value="0"/>
            <Option type="QString" name="trim_distance_start_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="trim_distance_start_unit" value="MM"/>
            <Option type="QString" name="tweak_dash_pattern_on_corners" value="0"/>
            <Option type="QString" name="use_custom_dash" value="0"/>
            <Option type="QString" name="width_map_unit_scale" value="3x:0,0,0,0,0,0"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option name="properties"/>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol type="line" frame_rate="10" clip_to_extent="1" force_rhr="0" alpha="1" is_animated="0" name="2">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" name="name" value=""/>
            <Option name="properties"/>
            <Option type="QString" name="type" value="collection"/>
          </Option>
        </data_defined_properties>
        <layer class="SimpleLine" enabled="1" locked="0" pass="3" id="{77fb6281-b05b-4475-a671-67b7ca3c588b}">
          <Option type="Map">
            <Option type="QString" name="align_dash_pattern" value="0"/>
            <Option type="QString" name="capstyle" value="round"/>
            <Option type="QString" name="customdash" value="5;2"/>
            <Option type="QString" name="customdash_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="customdash_unit" value="MM"/>
            <Option type="QString" name="dash_pattern_offset" value="0"/>
            <Option type="QString" name="dash_pattern_offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="dash_pattern_offset_unit" value="MM"/>
            <Option type="QString" name="draw_inside_polygon" value="0"/>
            <Option type="QString" name="joinstyle" value="bevel"/>
            <Option type="QString" name="line_color" value="214,133,34,255"/>
            <Option type="QString" name="line_style" value="solid"/>
            <Option type="QString" name="line_width" value="0.95"/>
            <Option type="QString" name="line_width_unit" value="MM"/>
            <Option type="QString" name="offset" value="0"/>
            <Option type="QString" name="offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="offset_unit" value="MM"/>
            <Option type="QString" name="ring_filter" value="0"/>
            <Option type="QString" name="trim_distance_end" value="0"/>
            <Option type="QString" name="trim_distance_end_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="trim_distance_end_unit" value="MM"/>
            <Option type="QString" name="trim_distance_start" value="0"/>
            <Option type="QString" name="trim_distance_start_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="trim_distance_start_unit" value="MM"/>
            <Option type="QString" name="tweak_dash_pattern_on_corners" value="0"/>
            <Option type="QString" name="use_custom_dash" value="0"/>
            <Option type="QString" name="width_map_unit_scale" value="3x:0,0,0,0,0,0"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option name="properties"/>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol type="line" frame_rate="10" clip_to_extent="1" force_rhr="0" alpha="1" is_animated="0" name="3">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" name="name" value=""/>
            <Option name="properties"/>
            <Option type="QString" name="type" value="collection"/>
          </Option>
        </data_defined_properties>
        <layer class="SimpleLine" enabled="1" locked="0" pass="2" id="{ef8fc544-122c-4324-b962-9bc341ad4d5d}">
          <Option type="Map">
            <Option type="QString" name="align_dash_pattern" value="0"/>
            <Option type="QString" name="capstyle" value="round"/>
            <Option type="QString" name="customdash" value="5;2"/>
            <Option type="QString" name="customdash_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="customdash_unit" value="MM"/>
            <Option type="QString" name="dash_pattern_offset" value="0"/>
            <Option type="QString" name="dash_pattern_offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="dash_pattern_offset_unit" value="MM"/>
            <Option type="QString" name="draw_inside_polygon" value="0"/>
            <Option type="QString" name="joinstyle" value="bevel"/>
            <Option type="QString" name="line_color" value="247,221,74,255"/>
            <Option type="QString" name="line_style" value="solid"/>
            <Option type="QString" name="line_width" value="0.9"/>
            <Option type="QString" name="line_width_unit" value="MM"/>
            <Option type="QString" name="offset" value="0"/>
            <Option type="QString" name="offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="offset_unit" value="MM"/>
            <Option type="QString" name="ring_filter" value="0"/>
            <Option type="QString" name="trim_distance_end" value="0"/>
            <Option type="QString" name="trim_distance_end_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="trim_distance_end_unit" value="MM"/>
            <Option type="QString" name="trim_distance_start" value="0"/>
            <Option type="QString" name="trim_distance_start_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="trim_distance_start_unit" value="MM"/>
            <Option type="QString" name="tweak_dash_pattern_on_corners" value="0"/>
            <Option type="QString" name="use_custom_dash" value="0"/>
            <Option type="QString" name="width_map_unit_scale" value="3x:0,0,0,0,0,0"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option name="properties"/>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol type="line" frame_rate="10" clip_to_extent="1" force_rhr="0" alpha="1" is_animated="0" name="4">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" name="name" value=""/>
            <Option name="properties"/>
            <Option type="QString" name="type" value="collection"/>
          </Option>
        </data_defined_properties>
        <layer class="SimpleLine" enabled="1" locked="0" pass="1" id="{cc2f3ea7-6a01-4345-9d4f-0d367f876706}">
          <Option type="Map">
            <Option type="QString" name="align_dash_pattern" value="0"/>
            <Option type="QString" name="capstyle" value="round"/>
            <Option type="QString" name="customdash" value="5;2"/>
            <Option type="QString" name="customdash_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="customdash_unit" value="MM"/>
            <Option type="QString" name="dash_pattern_offset" value="0"/>
            <Option type="QString" name="dash_pattern_offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="dash_pattern_offset_unit" value="MM"/>
            <Option type="QString" name="draw_inside_polygon" value="0"/>
            <Option type="QString" name="joinstyle" value="bevel"/>
            <Option type="QString" name="line_color" value="230,205,172,255"/>
            <Option type="QString" name="line_style" value="solid"/>
            <Option type="QString" name="line_width" value="0.65"/>
            <Option type="QString" name="line_width_unit" value="MM"/>
            <Option type="QString" name="offset" value="0"/>
            <Option type="QString" name="offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="offset_unit" value="MM"/>
            <Option type="QString" name="ring_filter" value="0"/>
            <Option type="QString" name="trim_distance_end" value="0"/>
            <Option type="QString" name="trim_distance_end_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="trim_distance_end_unit" value="MM"/>
            <Option type="QString" name="trim_distance_start" value="0"/>
            <Option type="QString" name="trim_distance_start_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="trim_distance_start_unit" value="MM"/>
            <Option type="QString" name="tweak_dash_pattern_on_corners" value="0"/>
            <Option type="QString" name="use_custom_dash" value="0"/>
            <Option type="QString" name="width_map_unit_scale" value="3x:0,0,0,0,0,0"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option name="properties"/>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </symbols>
  </renderer-v2>
  <labeling type="simple">
    <settings calloutType="simple">
      <text-style capitalization="0" allowHtml="0" useSubstitutions="0" fontSizeMapUnitScale="3x:0,0,0,0,0,0" fontFamily="Open Sans" fontKerning="1" namedStyle="Regular" blendMode="0" forcedBold="0" textOrientation="horizontal" multilineHeight="1" fontItalic="0" fontLetterSpacing="0" fontSize="10" textOpacity="1" fontStrikeout="0" fontWeight="50" multilineHeightUnit="Percentage" legendString="Aa" fieldName=" if( &quot;roadclassificationnumber&quot;, &quot;roadclassificationnumber&quot;, &quot;roadname&quot;)" fontWordSpacing="0" fontSizeUnit="Point" fontUnderline="0" isExpression="1" forcedItalic="0" textColor="50,50,50,255" previewBkgrdColor="255,255,255,255">
        <families/>
        <text-buffer bufferNoFill="1" bufferSize="1" bufferDraw="0" bufferBlendMode="0" bufferOpacity="1" bufferSizeMapUnitScale="3x:0,0,0,0,0,0" bufferColor="250,250,250,255" bufferJoinStyle="128" bufferSizeUnits="MM"/>
        <text-mask maskType="0" maskSizeUnits="MM" maskEnabled="0" maskSize="0" maskedSymbolLayers="" maskSizeMapUnitScale="3x:0,0,0,0,0,0" maskJoinStyle="128" maskOpacity="1"/>
        <background shapeRadiiUnit="Point" shapeBorderWidthUnit="Point" shapeJoinStyle="64" shapeOffsetMapUnitScale="3x:0,0,0,0,0,0" shapeRotation="0" shapeRotationType="0" shapeFillColor="255,255,255,255" shapeSizeX="0" shapeSizeType="0" shapeRadiiY="0" shapeType="0" shapeSVGFile="" shapeBorderWidth="0" shapeRadiiMapUnitScale="3x:0,0,0,0,0,0" shapeOffsetUnit="Point" shapeOpacity="1" shapeRadiiX="0" shapeSizeY="0" shapeDraw="0" shapeBorderWidthMapUnitScale="3x:0,0,0,0,0,0" shapeSizeUnit="Point" shapeOffsetX="0" shapeSizeMapUnitScale="3x:0,0,0,0,0,0" shapeOffsetY="0" shapeBorderColor="128,128,128,255" shapeBlendMode="0">
          <symbol type="marker" frame_rate="10" clip_to_extent="1" force_rhr="0" alpha="1" is_animated="0" name="markerSymbol">
            <data_defined_properties>
              <Option type="Map">
                <Option type="QString" name="name" value=""/>
                <Option name="properties"/>
                <Option type="QString" name="type" value="collection"/>
              </Option>
            </data_defined_properties>
            <layer class="SimpleMarker" enabled="1" locked="0" pass="0" id="">
              <Option type="Map">
                <Option type="QString" name="angle" value="0"/>
                <Option type="QString" name="cap_style" value="square"/>
                <Option type="QString" name="color" value="190,207,80,255"/>
                <Option type="QString" name="horizontal_anchor_point" value="1"/>
                <Option type="QString" name="joinstyle" value="bevel"/>
                <Option type="QString" name="name" value="circle"/>
                <Option type="QString" name="offset" value="0,0"/>
                <Option type="QString" name="offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
                <Option type="QString" name="offset_unit" value="MM"/>
                <Option type="QString" name="outline_color" value="35,35,35,255"/>
                <Option type="QString" name="outline_style" value="solid"/>
                <Option type="QString" name="outline_width" value="0"/>
                <Option type="QString" name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0"/>
                <Option type="QString" name="outline_width_unit" value="MM"/>
                <Option type="QString" name="scale_method" value="diameter"/>
                <Option type="QString" name="size" value="2"/>
                <Option type="QString" name="size_map_unit_scale" value="3x:0,0,0,0,0,0"/>
                <Option type="QString" name="size_unit" value="MM"/>
                <Option type="QString" name="vertical_anchor_point" value="1"/>
              </Option>
              <data_defined_properties>
                <Option type="Map">
                  <Option type="QString" name="name" value=""/>
                  <Option name="properties"/>
                  <Option type="QString" name="type" value="collection"/>
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
          <symbol type="fill" frame_rate="10" clip_to_extent="1" force_rhr="0" alpha="1" is_animated="0" name="fillSymbol">
            <data_defined_properties>
              <Option type="Map">
                <Option type="QString" name="name" value=""/>
                <Option name="properties"/>
                <Option type="QString" name="type" value="collection"/>
              </Option>
            </data_defined_properties>
            <layer class="SimpleFill" enabled="1" locked="0" pass="0" id="">
              <Option type="Map">
                <Option type="QString" name="border_width_map_unit_scale" value="3x:0,0,0,0,0,0"/>
                <Option type="QString" name="color" value="255,255,255,255"/>
                <Option type="QString" name="joinstyle" value="bevel"/>
                <Option type="QString" name="offset" value="0,0"/>
                <Option type="QString" name="offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
                <Option type="QString" name="offset_unit" value="MM"/>
                <Option type="QString" name="outline_color" value="128,128,128,255"/>
                <Option type="QString" name="outline_style" value="no"/>
                <Option type="QString" name="outline_width" value="0"/>
                <Option type="QString" name="outline_width_unit" value="Point"/>
                <Option type="QString" name="style" value="solid"/>
              </Option>
              <data_defined_properties>
                <Option type="Map">
                  <Option type="QString" name="name" value=""/>
                  <Option name="properties"/>
                  <Option type="QString" name="type" value="collection"/>
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </background>
        <shadow shadowOffsetAngle="135" shadowRadius="1.5" shadowColor="0,0,0,255" shadowOffsetUnit="MM" shadowScale="100" shadowDraw="0" shadowUnder="0" shadowRadiusAlphaOnly="0" shadowOpacity="0.69999999999999996" shadowOffsetDist="1" shadowRadiusMapUnitScale="3x:0,0,0,0,0,0" shadowOffsetMapUnitScale="3x:0,0,0,0,0,0" shadowOffsetGlobal="1" shadowBlendMode="6" shadowRadiusUnit="MM"/>
        <dd_properties>
          <Option type="Map">
            <Option type="QString" name="name" value=""/>
            <Option name="properties"/>
            <Option type="QString" name="type" value="collection"/>
          </Option>
        </dd_properties>
        <substitutions/>
      </text-style>
      <text-format wrapChar="" useMaxLineLengthForAutoWrap="1" rightDirectionSymbol=">" addDirectionSymbol="0" reverseDirectionSymbol="0" placeDirectionSymbol="0" multilineAlign="0" leftDirectionSymbol="&lt;" formatNumbers="0" plussign="0" decimals="3" autoWrapLength="0"/>
      <placement geometryGeneratorType="PointGeometry" predefinedPositionOrder="TR,TL,BR,BL,R,L,TSR,BSR" centroidInside="0" xOffset="0" geometryGenerator="" geometryGeneratorEnabled="0" lineAnchorPercent="0.5" overlapHandling="PreventOverlap" distMapUnitScale="3x:0,0,0,0,0,0" yOffset="0" centroidWhole="0" polygonPlacementFlags="2" repeatDistanceUnits="MM" dist="0" offsetUnits="MM" labelOffsetMapUnitScale="3x:0,0,0,0,0,0" allowDegraded="0" placement="3" lineAnchorTextPoint="FollowPlacement" repeatDistance="0" overrunDistance="0" preserveRotation="1" lineAnchorType="0" rotationAngle="0" maxCurvedCharAngleIn="25" overrunDistanceUnit="MM" rotationUnit="AngleDegrees" fitInPolygonOnly="0" maxCurvedCharAngleOut="-25" placementFlags="10" quadOffset="4" repeatDistanceMapUnitScale="3x:0,0,0,0,0,0" offsetType="0" overrunDistanceMapUnitScale="3x:0,0,0,0,0,0" layerType="LineGeometry" priority="5" lineAnchorClipping="0" distUnits="MM"/>
      <rendering scaleMin="100" scaleVisibility="1" drawLabels="1" obstacleFactor="1" obstacle="1" fontLimitPixelSize="0" zIndex="0" mergeLines="1" obstacleType="1" fontMaxPixelSize="10000" labelPerPart="0" minFeatureSize="0" fontMinPixelSize="3" upsidedownLabels="0" scaleMax="6000" limitNumLabels="0" unplacedVisibility="0" maxNumLabels="2000"/>
      <dd_properties>
        <Option type="Map">
          <Option type="QString" name="name" value=""/>
          <Option name="properties"/>
          <Option type="QString" name="type" value="collection"/>
        </Option>
      </dd_properties>
      <callout type="simple">
        <Option type="Map">
          <Option type="QString" name="anchorPoint" value="pole_of_inaccessibility"/>
          <Option type="int" name="blendMode" value="0"/>
          <Option type="Map" name="ddProperties">
            <Option type="QString" name="name" value=""/>
            <Option name="properties"/>
            <Option type="QString" name="type" value="collection"/>
          </Option>
          <Option type="bool" name="drawToAllParts" value="false"/>
          <Option type="QString" name="enabled" value="0"/>
          <Option type="QString" name="labelAnchorPoint" value="point_on_exterior"/>
          <Option type="QString" name="lineSymbol" value="&lt;symbol type=&quot;line&quot; frame_rate=&quot;10&quot; clip_to_extent=&quot;1&quot; force_rhr=&quot;0&quot; alpha=&quot;1&quot; is_animated=&quot;0&quot; name=&quot;symbol&quot;>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; name=&quot;name&quot; value=&quot;&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;type&quot; value=&quot;collection&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;layer class=&quot;SimpleLine&quot; enabled=&quot;1&quot; locked=&quot;0&quot; pass=&quot;0&quot; id=&quot;{1ee2442f-22a4-4708-943a-4d087f844505}&quot;>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; name=&quot;align_dash_pattern&quot; value=&quot;0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;capstyle&quot; value=&quot;square&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;customdash&quot; value=&quot;5;2&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;customdash_map_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;customdash_unit&quot; value=&quot;MM&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;dash_pattern_offset&quot; value=&quot;0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;dash_pattern_offset_map_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;dash_pattern_offset_unit&quot; value=&quot;MM&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;draw_inside_polygon&quot; value=&quot;0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;joinstyle&quot; value=&quot;bevel&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;line_color&quot; value=&quot;60,60,60,255&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;line_style&quot; value=&quot;solid&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;line_width&quot; value=&quot;0.3&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;line_width_unit&quot; value=&quot;MM&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;offset&quot; value=&quot;0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;offset_map_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;offset_unit&quot; value=&quot;MM&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;ring_filter&quot; value=&quot;0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;trim_distance_end&quot; value=&quot;0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;trim_distance_end_map_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;trim_distance_end_unit&quot; value=&quot;MM&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;trim_distance_start&quot; value=&quot;0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;trim_distance_start_map_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;trim_distance_start_unit&quot; value=&quot;MM&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;tweak_dash_pattern_on_corners&quot; value=&quot;0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;use_custom_dash&quot; value=&quot;0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;width_map_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;/Option>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; name=&quot;name&quot; value=&quot;&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;type&quot; value=&quot;collection&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;/layer>&lt;/symbol>"/>
          <Option type="double" name="minLength" value="0"/>
          <Option type="QString" name="minLengthMapUnitScale" value="3x:0,0,0,0,0,0"/>
          <Option type="QString" name="minLengthUnit" value="MM"/>
          <Option type="double" name="offsetFromAnchor" value="0"/>
          <Option type="QString" name="offsetFromAnchorMapUnitScale" value="3x:0,0,0,0,0,0"/>
          <Option type="QString" name="offsetFromAnchorUnit" value="MM"/>
          <Option type="double" name="offsetFromLabel" value="0"/>
          <Option type="QString" name="offsetFromLabelMapUnitScale" value="3x:0,0,0,0,0,0"/>
          <Option type="QString" name="offsetFromLabelUnit" value="MM"/>
        </Option>
      </callout>
    </settings>
  </labeling>
  <customproperties>
    <Option type="Map">
      <Option type="List" name="dualview/previewExpressions">
        <Option type="QString" value="&quot;namespace&quot;"/>
      </Option>
      <Option type="QString" name="embeddedWidgets/count" value="0"/>
      <Option name="variableNames"/>
      <Option name="variableValues"/>
    </Option>
  </customproperties>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerOpacity>1</layerOpacity>
  <SingleCategoryDiagramRenderer diagramType="Histogram" attributeLegend="1">
    <DiagramCategory spacingUnitScale="3x:0,0,0,0,0,0" direction="1" minScaleDenominator="0" scaleBasedVisibility="0" backgroundColor="#ffffff" diagramOrientation="Up" scaleDependency="Area" showAxis="0" opacity="1" sizeScale="3x:0,0,0,0,0,0" minimumSize="0" backgroundAlpha="255" enabled="0" penWidth="0" penColor="#000000" spacingUnit="MM" barWidth="5" lineSizeType="MM" penAlpha="255" maxScaleDenominator="1e+08" sizeType="MM" rotationOffset="270" labelPlacementMethod="XHeight" width="15" spacing="0" lineSizeScale="3x:0,0,0,0,0,0" height="15">
      <fontProperties bold="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" strikethrough="0" underline="0" italic="0" style=""/>
      <attribute color="#000000" field="" colorOpacity="1" label=""/>
      <axisSymbol>
        <symbol type="line" frame_rate="10" clip_to_extent="1" force_rhr="0" alpha="1" is_animated="0" name="">
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option name="properties"/>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </data_defined_properties>
          <layer class="SimpleLine" enabled="1" locked="0" pass="0" id="{efbbebbd-a880-49c9-ad58-547236978253}">
            <Option type="Map">
              <Option type="QString" name="align_dash_pattern" value="0"/>
              <Option type="QString" name="capstyle" value="square"/>
              <Option type="QString" name="customdash" value="5;2"/>
              <Option type="QString" name="customdash_map_unit_scale" value="3x:0,0,0,0,0,0"/>
              <Option type="QString" name="customdash_unit" value="MM"/>
              <Option type="QString" name="dash_pattern_offset" value="0"/>
              <Option type="QString" name="dash_pattern_offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
              <Option type="QString" name="dash_pattern_offset_unit" value="MM"/>
              <Option type="QString" name="draw_inside_polygon" value="0"/>
              <Option type="QString" name="joinstyle" value="bevel"/>
              <Option type="QString" name="line_color" value="35,35,35,255"/>
              <Option type="QString" name="line_style" value="solid"/>
              <Option type="QString" name="line_width" value="0.26"/>
              <Option type="QString" name="line_width_unit" value="MM"/>
              <Option type="QString" name="offset" value="0"/>
              <Option type="QString" name="offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
              <Option type="QString" name="offset_unit" value="MM"/>
              <Option type="QString" name="ring_filter" value="0"/>
              <Option type="QString" name="trim_distance_end" value="0"/>
              <Option type="QString" name="trim_distance_end_map_unit_scale" value="3x:0,0,0,0,0,0"/>
              <Option type="QString" name="trim_distance_end_unit" value="MM"/>
              <Option type="QString" name="trim_distance_start" value="0"/>
              <Option type="QString" name="trim_distance_start_map_unit_scale" value="3x:0,0,0,0,0,0"/>
              <Option type="QString" name="trim_distance_start_unit" value="MM"/>
              <Option type="QString" name="tweak_dash_pattern_on_corners" value="0"/>
              <Option type="QString" name="use_custom_dash" value="0"/>
              <Option type="QString" name="width_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            </Option>
            <data_defined_properties>
              <Option type="Map">
                <Option type="QString" name="name" value=""/>
                <Option name="properties"/>
                <Option type="QString" name="type" value="collection"/>
              </Option>
            </data_defined_properties>
          </layer>
        </symbol>
      </axisSymbol>
    </DiagramCategory>
  </SingleCategoryDiagramRenderer>
  <DiagramLayerSettings placement="2" dist="0" linePlacementFlags="2" showAll="1" obstacle="0" priority="0" zIndex="0">
    <properties>
      <Option type="Map">
        <Option type="QString" name="name" value=""/>
        <Option name="properties"/>
        <Option type="QString" name="type" value="collection"/>
      </Option>
    </properties>
  </DiagramLayerSettings>
  <geometryOptions geometryPrecision="0" removeDuplicateNodes="0">
    <activeChecks/>
    <checkConfiguration/>
  </geometryOptions>
  <legend showLabelLegend="0" type="default-vector"/>
  <referencedLayers/>
  <fieldConfiguration>
    <field configurationFlags="None" name="ogc_fid">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="gml_id">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="identifier">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="localid">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="namespace">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="beginlifespanversion">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="fictitious">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="validfrom">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="reasonforchange">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="roadclassification">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="routehierarchy">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="formofway">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="trunkroad">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="primaryroute">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="roadclassificationnumber">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="roadname">
      <editWidget type="List">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="roadnamelang">
      <editWidget type="List">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="alternatename">
      <editWidget type="List">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="alternatenamelang">
      <editWidget type="List">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="operationalstate">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="provenance">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="directionalitytitle">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="length">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="matchstatus">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="alternateidentifieridentifier">
      <editWidget type="List">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="alternateidentifieridentifierscheme">
      <editWidget type="List">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="startgradeseparation">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="endgradeseparation">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="roadstructure">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="cyclefacility">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="wholelink">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="averagewidth">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="minimumwidth">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="confidencelevel">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="numberoflanes">
      <editWidget type="List">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="direction">
      <editWidget type="List">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="minmaxnumberoflanes">
      <editWidget type="List">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="indirection">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="inoppositedirection">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="formspartofrole">
      <editWidget type="List">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="formspartofhref">
      <editWidget type="List">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="startnodehref">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="endnodehref">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="relatedroadareahref">
      <editWidget type="List">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="TOLL_P">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias field="ogc_fid" index="0" name=""/>
    <alias field="gml_id" index="1" name=""/>
    <alias field="identifier" index="2" name=""/>
    <alias field="localid" index="3" name=""/>
    <alias field="namespace" index="4" name=""/>
    <alias field="beginlifespanversion" index="5" name=""/>
    <alias field="fictitious" index="6" name=""/>
    <alias field="validfrom" index="7" name=""/>
    <alias field="reasonforchange" index="8" name=""/>
    <alias field="roadclassification" index="9" name=""/>
    <alias field="routehierarchy" index="10" name=""/>
    <alias field="formofway" index="11" name=""/>
    <alias field="trunkroad" index="12" name=""/>
    <alias field="primaryroute" index="13" name=""/>
    <alias field="roadclassificationnumber" index="14" name=""/>
    <alias field="roadname" index="15" name=""/>
    <alias field="roadnamelang" index="16" name=""/>
    <alias field="alternatename" index="17" name=""/>
    <alias field="alternatenamelang" index="18" name=""/>
    <alias field="operationalstate" index="19" name=""/>
    <alias field="provenance" index="20" name=""/>
    <alias field="directionalitytitle" index="21" name=""/>
    <alias field="length" index="22" name=""/>
    <alias field="matchstatus" index="23" name=""/>
    <alias field="alternateidentifieridentifier" index="24" name=""/>
    <alias field="alternateidentifieridentifierscheme" index="25" name=""/>
    <alias field="startgradeseparation" index="26" name=""/>
    <alias field="endgradeseparation" index="27" name=""/>
    <alias field="roadstructure" index="28" name=""/>
    <alias field="cyclefacility" index="29" name=""/>
    <alias field="wholelink" index="30" name=""/>
    <alias field="averagewidth" index="31" name=""/>
    <alias field="minimumwidth" index="32" name=""/>
    <alias field="confidencelevel" index="33" name=""/>
    <alias field="numberoflanes" index="34" name=""/>
    <alias field="direction" index="35" name=""/>
    <alias field="minmaxnumberoflanes" index="36" name=""/>
    <alias field="indirection" index="37" name=""/>
    <alias field="inoppositedirection" index="38" name=""/>
    <alias field="formspartofrole" index="39" name=""/>
    <alias field="formspartofhref" index="40" name=""/>
    <alias field="startnodehref" index="41" name=""/>
    <alias field="endnodehref" index="42" name=""/>
    <alias field="relatedroadareahref" index="43" name=""/>
    <alias field="TOLL_P" index="44" name=""/>
  </aliases>
  <splitPolicies>
    <policy field="ogc_fid" policy="Duplicate"/>
    <policy field="gml_id" policy="Duplicate"/>
    <policy field="identifier" policy="Duplicate"/>
    <policy field="localid" policy="Duplicate"/>
    <policy field="namespace" policy="Duplicate"/>
    <policy field="beginlifespanversion" policy="Duplicate"/>
    <policy field="fictitious" policy="Duplicate"/>
    <policy field="validfrom" policy="Duplicate"/>
    <policy field="reasonforchange" policy="Duplicate"/>
    <policy field="roadclassification" policy="Duplicate"/>
    <policy field="routehierarchy" policy="Duplicate"/>
    <policy field="formofway" policy="Duplicate"/>
    <policy field="trunkroad" policy="Duplicate"/>
    <policy field="primaryroute" policy="Duplicate"/>
    <policy field="roadclassificationnumber" policy="Duplicate"/>
    <policy field="roadname" policy="Duplicate"/>
    <policy field="roadnamelang" policy="Duplicate"/>
    <policy field="alternatename" policy="Duplicate"/>
    <policy field="alternatenamelang" policy="Duplicate"/>
    <policy field="operationalstate" policy="Duplicate"/>
    <policy field="provenance" policy="Duplicate"/>
    <policy field="directionalitytitle" policy="Duplicate"/>
    <policy field="length" policy="Duplicate"/>
    <policy field="matchstatus" policy="Duplicate"/>
    <policy field="alternateidentifieridentifier" policy="Duplicate"/>
    <policy field="alternateidentifieridentifierscheme" policy="Duplicate"/>
    <policy field="startgradeseparation" policy="Duplicate"/>
    <policy field="endgradeseparation" policy="Duplicate"/>
    <policy field="roadstructure" policy="Duplicate"/>
    <policy field="cyclefacility" policy="Duplicate"/>
    <policy field="wholelink" policy="Duplicate"/>
    <policy field="averagewidth" policy="Duplicate"/>
    <policy field="minimumwidth" policy="Duplicate"/>
    <policy field="confidencelevel" policy="Duplicate"/>
    <policy field="numberoflanes" policy="Duplicate"/>
    <policy field="direction" policy="Duplicate"/>
    <policy field="minmaxnumberoflanes" policy="Duplicate"/>
    <policy field="indirection" policy="Duplicate"/>
    <policy field="inoppositedirection" policy="Duplicate"/>
    <policy field="formspartofrole" policy="Duplicate"/>
    <policy field="formspartofhref" policy="Duplicate"/>
    <policy field="startnodehref" policy="Duplicate"/>
    <policy field="endnodehref" policy="Duplicate"/>
    <policy field="relatedroadareahref" policy="Duplicate"/>
    <policy field="TOLL_P" policy="Duplicate"/>
  </splitPolicies>
  <defaults>
    <default applyOnUpdate="0" field="ogc_fid" expression=""/>
    <default applyOnUpdate="0" field="gml_id" expression=""/>
    <default applyOnUpdate="0" field="identifier" expression=""/>
    <default applyOnUpdate="0" field="localid" expression=""/>
    <default applyOnUpdate="0" field="namespace" expression=""/>
    <default applyOnUpdate="0" field="beginlifespanversion" expression=""/>
    <default applyOnUpdate="0" field="fictitious" expression=""/>
    <default applyOnUpdate="0" field="validfrom" expression=""/>
    <default applyOnUpdate="0" field="reasonforchange" expression=""/>
    <default applyOnUpdate="0" field="roadclassification" expression=""/>
    <default applyOnUpdate="0" field="routehierarchy" expression=""/>
    <default applyOnUpdate="0" field="formofway" expression=""/>
    <default applyOnUpdate="0" field="trunkroad" expression=""/>
    <default applyOnUpdate="0" field="primaryroute" expression=""/>
    <default applyOnUpdate="0" field="roadclassificationnumber" expression=""/>
    <default applyOnUpdate="0" field="roadname" expression=""/>
    <default applyOnUpdate="0" field="roadnamelang" expression=""/>
    <default applyOnUpdate="0" field="alternatename" expression=""/>
    <default applyOnUpdate="0" field="alternatenamelang" expression=""/>
    <default applyOnUpdate="0" field="operationalstate" expression=""/>
    <default applyOnUpdate="0" field="provenance" expression=""/>
    <default applyOnUpdate="0" field="directionalitytitle" expression=""/>
    <default applyOnUpdate="0" field="length" expression=""/>
    <default applyOnUpdate="0" field="matchstatus" expression=""/>
    <default applyOnUpdate="0" field="alternateidentifieridentifier" expression=""/>
    <default applyOnUpdate="0" field="alternateidentifieridentifierscheme" expression=""/>
    <default applyOnUpdate="0" field="startgradeseparation" expression=""/>
    <default applyOnUpdate="0" field="endgradeseparation" expression=""/>
    <default applyOnUpdate="0" field="roadstructure" expression=""/>
    <default applyOnUpdate="0" field="cyclefacility" expression=""/>
    <default applyOnUpdate="0" field="wholelink" expression=""/>
    <default applyOnUpdate="0" field="averagewidth" expression=""/>
    <default applyOnUpdate="0" field="minimumwidth" expression=""/>
    <default applyOnUpdate="0" field="confidencelevel" expression=""/>
    <default applyOnUpdate="0" field="numberoflanes" expression=""/>
    <default applyOnUpdate="0" field="direction" expression=""/>
    <default applyOnUpdate="0" field="minmaxnumberoflanes" expression=""/>
    <default applyOnUpdate="0" field="indirection" expression=""/>
    <default applyOnUpdate="0" field="inoppositedirection" expression=""/>
    <default applyOnUpdate="0" field="formspartofrole" expression=""/>
    <default applyOnUpdate="0" field="formspartofhref" expression=""/>
    <default applyOnUpdate="0" field="startnodehref" expression=""/>
    <default applyOnUpdate="0" field="endnodehref" expression=""/>
    <default applyOnUpdate="0" field="relatedroadareahref" expression=""/>
    <default applyOnUpdate="0" field="TOLL_P" expression=""/>
  </defaults>
  <constraints>
    <constraint unique_strength="0" field="ogc_fid" constraints="0" notnull_strength="0" exp_strength="0"/>
    <constraint unique_strength="0" field="gml_id" constraints="0" notnull_strength="0" exp_strength="0"/>
    <constraint unique_strength="0" field="identifier" constraints="0" notnull_strength="0" exp_strength="0"/>
    <constraint unique_strength="0" field="localid" constraints="0" notnull_strength="0" exp_strength="0"/>
    <constraint unique_strength="0" field="namespace" constraints="0" notnull_strength="0" exp_strength="0"/>
    <constraint unique_strength="0" field="beginlifespanversion" constraints="0" notnull_strength="0" exp_strength="0"/>
    <constraint unique_strength="0" field="fictitious" constraints="0" notnull_strength="0" exp_strength="0"/>
    <constraint unique_strength="0" field="validfrom" constraints="0" notnull_strength="0" exp_strength="0"/>
    <constraint unique_strength="0" field="reasonforchange" constraints="0" notnull_strength="0" exp_strength="0"/>
    <constraint unique_strength="0" field="roadclassification" constraints="0" notnull_strength="0" exp_strength="0"/>
    <constraint unique_strength="0" field="routehierarchy" constraints="0" notnull_strength="0" exp_strength="0"/>
    <constraint unique_strength="0" field="formofway" constraints="0" notnull_strength="0" exp_strength="0"/>
    <constraint unique_strength="0" field="trunkroad" constraints="0" notnull_strength="0" exp_strength="0"/>
    <constraint unique_strength="0" field="primaryroute" constraints="0" notnull_strength="0" exp_strength="0"/>
    <constraint unique_strength="0" field="roadclassificationnumber" constraints="0" notnull_strength="0" exp_strength="0"/>
    <constraint unique_strength="0" field="roadname" constraints="0" notnull_strength="0" exp_strength="0"/>
    <constraint unique_strength="0" field="roadnamelang" constraints="0" notnull_strength="0" exp_strength="0"/>
    <constraint unique_strength="0" field="alternatename" constraints="0" notnull_strength="0" exp_strength="0"/>
    <constraint unique_strength="0" field="alternatenamelang" constraints="0" notnull_strength="0" exp_strength="0"/>
    <constraint unique_strength="0" field="operationalstate" constraints="0" notnull_strength="0" exp_strength="0"/>
    <constraint unique_strength="0" field="provenance" constraints="0" notnull_strength="0" exp_strength="0"/>
    <constraint unique_strength="0" field="directionalitytitle" constraints="0" notnull_strength="0" exp_strength="0"/>
    <constraint unique_strength="0" field="length" constraints="0" notnull_strength="0" exp_strength="0"/>
    <constraint unique_strength="0" field="matchstatus" constraints="0" notnull_strength="0" exp_strength="0"/>
    <constraint unique_strength="0" field="alternateidentifieridentifier" constraints="0" notnull_strength="0" exp_strength="0"/>
    <constraint unique_strength="0" field="alternateidentifieridentifierscheme" constraints="0" notnull_strength="0" exp_strength="0"/>
    <constraint unique_strength="0" field="startgradeseparation" constraints="0" notnull_strength="0" exp_strength="0"/>
    <constraint unique_strength="0" field="endgradeseparation" constraints="0" notnull_strength="0" exp_strength="0"/>
    <constraint unique_strength="0" field="roadstructure" constraints="0" notnull_strength="0" exp_strength="0"/>
    <constraint unique_strength="0" field="cyclefacility" constraints="0" notnull_strength="0" exp_strength="0"/>
    <constraint unique_strength="0" field="wholelink" constraints="0" notnull_strength="0" exp_strength="0"/>
    <constraint unique_strength="0" field="averagewidth" constraints="0" notnull_strength="0" exp_strength="0"/>
    <constraint unique_strength="0" field="minimumwidth" constraints="0" notnull_strength="0" exp_strength="0"/>
    <constraint unique_strength="0" field="confidencelevel" constraints="0" notnull_strength="0" exp_strength="0"/>
    <constraint unique_strength="0" field="numberoflanes" constraints="0" notnull_strength="0" exp_strength="0"/>
    <constraint unique_strength="0" field="direction" constraints="0" notnull_strength="0" exp_strength="0"/>
    <constraint unique_strength="0" field="minmaxnumberoflanes" constraints="0" notnull_strength="0" exp_strength="0"/>
    <constraint unique_strength="0" field="indirection" constraints="0" notnull_strength="0" exp_strength="0"/>
    <constraint unique_strength="0" field="inoppositedirection" constraints="0" notnull_strength="0" exp_strength="0"/>
    <constraint unique_strength="0" field="formspartofrole" constraints="0" notnull_strength="0" exp_strength="0"/>
    <constraint unique_strength="0" field="formspartofhref" constraints="0" notnull_strength="0" exp_strength="0"/>
    <constraint unique_strength="0" field="startnodehref" constraints="0" notnull_strength="0" exp_strength="0"/>
    <constraint unique_strength="0" field="endnodehref" constraints="0" notnull_strength="0" exp_strength="0"/>
    <constraint unique_strength="0" field="relatedroadareahref" constraints="0" notnull_strength="0" exp_strength="0"/>
    <constraint unique_strength="0" field="TOLL_P" constraints="0" notnull_strength="0" exp_strength="0"/>
  </constraints>
  <constraintExpressions>
    <constraint desc="" field="ogc_fid" exp=""/>
    <constraint desc="" field="gml_id" exp=""/>
    <constraint desc="" field="identifier" exp=""/>
    <constraint desc="" field="localid" exp=""/>
    <constraint desc="" field="namespace" exp=""/>
    <constraint desc="" field="beginlifespanversion" exp=""/>
    <constraint desc="" field="fictitious" exp=""/>
    <constraint desc="" field="validfrom" exp=""/>
    <constraint desc="" field="reasonforchange" exp=""/>
    <constraint desc="" field="roadclassification" exp=""/>
    <constraint desc="" field="routehierarchy" exp=""/>
    <constraint desc="" field="formofway" exp=""/>
    <constraint desc="" field="trunkroad" exp=""/>
    <constraint desc="" field="primaryroute" exp=""/>
    <constraint desc="" field="roadclassificationnumber" exp=""/>
    <constraint desc="" field="roadname" exp=""/>
    <constraint desc="" field="roadnamelang" exp=""/>
    <constraint desc="" field="alternatename" exp=""/>
    <constraint desc="" field="alternatenamelang" exp=""/>
    <constraint desc="" field="operationalstate" exp=""/>
    <constraint desc="" field="provenance" exp=""/>
    <constraint desc="" field="directionalitytitle" exp=""/>
    <constraint desc="" field="length" exp=""/>
    <constraint desc="" field="matchstatus" exp=""/>
    <constraint desc="" field="alternateidentifieridentifier" exp=""/>
    <constraint desc="" field="alternateidentifieridentifierscheme" exp=""/>
    <constraint desc="" field="startgradeseparation" exp=""/>
    <constraint desc="" field="endgradeseparation" exp=""/>
    <constraint desc="" field="roadstructure" exp=""/>
    <constraint desc="" field="cyclefacility" exp=""/>
    <constraint desc="" field="wholelink" exp=""/>
    <constraint desc="" field="averagewidth" exp=""/>
    <constraint desc="" field="minimumwidth" exp=""/>
    <constraint desc="" field="confidencelevel" exp=""/>
    <constraint desc="" field="numberoflanes" exp=""/>
    <constraint desc="" field="direction" exp=""/>
    <constraint desc="" field="minmaxnumberoflanes" exp=""/>
    <constraint desc="" field="indirection" exp=""/>
    <constraint desc="" field="inoppositedirection" exp=""/>
    <constraint desc="" field="formspartofrole" exp=""/>
    <constraint desc="" field="formspartofhref" exp=""/>
    <constraint desc="" field="startnodehref" exp=""/>
    <constraint desc="" field="endnodehref" exp=""/>
    <constraint desc="" field="relatedroadareahref" exp=""/>
    <constraint desc="" field="TOLL_P" exp=""/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction key="Canvas" value="{00000000-0000-0000-0000-000000000000}"/>
  </attributeactions>
  <attributetableconfig sortOrder="0" sortExpression="" actionWidgetStyle="dropDown">
    <columns>
      <column hidden="0" type="field" width="-1" name="identifier"/>
      <column hidden="0" type="field" width="-1" name="fictitious"/>
      <column hidden="0" type="field" width="-1" name="provenance"/>
      <column hidden="0" type="field" width="-1" name="length"/>
      <column hidden="0" type="field" width="-1" name="ogc_fid"/>
      <column hidden="0" type="field" width="-1" name="gml_id"/>
      <column hidden="0" type="field" width="-1" name="localid"/>
      <column hidden="0" type="field" width="-1" name="namespace"/>
      <column hidden="0" type="field" width="-1" name="beginlifespanversion"/>
      <column hidden="0" type="field" width="-1" name="validfrom"/>
      <column hidden="0" type="field" width="-1" name="reasonforchange"/>
      <column hidden="0" type="field" width="-1" name="roadclassification"/>
      <column hidden="0" type="field" width="-1" name="routehierarchy"/>
      <column hidden="0" type="field" width="-1" name="formofway"/>
      <column hidden="0" type="field" width="-1" name="trunkroad"/>
      <column hidden="0" type="field" width="-1" name="primaryroute"/>
      <column hidden="0" type="field" width="-1" name="roadclassificationnumber"/>
      <column hidden="0" type="field" width="-1" name="roadname"/>
      <column hidden="0" type="field" width="-1" name="roadnamelang"/>
      <column hidden="0" type="field" width="-1" name="alternatename"/>
      <column hidden="0" type="field" width="-1" name="alternatenamelang"/>
      <column hidden="0" type="field" width="-1" name="operationalstate"/>
      <column hidden="0" type="field" width="-1" name="directionalitytitle"/>
      <column hidden="0" type="field" width="-1" name="matchstatus"/>
      <column hidden="0" type="field" width="-1" name="alternateidentifieridentifier"/>
      <column hidden="0" type="field" width="-1" name="alternateidentifieridentifierscheme"/>
      <column hidden="0" type="field" width="-1" name="startgradeseparation"/>
      <column hidden="0" type="field" width="-1" name="endgradeseparation"/>
      <column hidden="0" type="field" width="-1" name="roadstructure"/>
      <column hidden="0" type="field" width="-1" name="cyclefacility"/>
      <column hidden="0" type="field" width="-1" name="wholelink"/>
      <column hidden="0" type="field" width="-1" name="averagewidth"/>
      <column hidden="0" type="field" width="-1" name="minimumwidth"/>
      <column hidden="0" type="field" width="-1" name="confidencelevel"/>
      <column hidden="0" type="field" width="-1" name="numberoflanes"/>
      <column hidden="0" type="field" width="-1" name="direction"/>
      <column hidden="0" type="field" width="-1" name="minmaxnumberoflanes"/>
      <column hidden="0" type="field" width="-1" name="indirection"/>
      <column hidden="0" type="field" width="-1" name="inoppositedirection"/>
      <column hidden="0" type="field" width="-1" name="formspartofrole"/>
      <column hidden="0" type="field" width="-1" name="formspartofhref"/>
      <column hidden="0" type="field" width="-1" name="startnodehref"/>
      <column hidden="0" type="field" width="-1" name="endnodehref"/>
      <column hidden="0" type="field" width="-1" name="relatedroadareahref"/>
      <column hidden="0" type="field" width="-1" name="TOLL_P"/>
      <column hidden="1" type="actions" width="-1"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <storedexpressions/>
  <editform tolerant="1">C:/Users/Student/Documents</editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath>C:/Users/Student/Documents</editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
QGIS forms can have a Python function that is called when the form is
opened.

Use this function to add extra logic to your forms.

Enter the name of the function in the "Python Init function"
field.
An example follows:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
	geom = feature.geometry()
	control = dialog.findChild(QWidget, "MyLineEdit")
]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>generatedlayout</editorlayout>
  <editable>
    <field name="TOLL_P" editable="1"/>
    <field name="alternateidentifieridentifier" editable="1"/>
    <field name="alternateidentifieridentifierscheme" editable="1"/>
    <field name="alternatename" editable="1"/>
    <field name="alternatenamelang" editable="1"/>
    <field name="averagewidth" editable="1"/>
    <field name="beginlifespanversion" editable="1"/>
    <field name="confidencelevel" editable="1"/>
    <field name="cyclefacility" editable="1"/>
    <field name="direction" editable="1"/>
    <field name="directionalitytitle" editable="1"/>
    <field name="endgradeseparation" editable="1"/>
    <field name="endnodehref" editable="1"/>
    <field name="fictitious" editable="1"/>
    <field name="formofway" editable="1"/>
    <field name="formspartofhref" editable="1"/>
    <field name="formspartofrole" editable="1"/>
    <field name="gml_id" editable="1"/>
    <field name="identifier" editable="1"/>
    <field name="indirection" editable="1"/>
    <field name="inoppositedirection" editable="1"/>
    <field name="length" editable="1"/>
    <field name="localid" editable="1"/>
    <field name="matchstatus" editable="1"/>
    <field name="minimumwidth" editable="1"/>
    <field name="minmaxnumberoflanes" editable="1"/>
    <field name="namespace" editable="1"/>
    <field name="numberoflanes" editable="1"/>
    <field name="ogc_fid" editable="1"/>
    <field name="operationalstate" editable="1"/>
    <field name="primaryroute" editable="1"/>
    <field name="provenance" editable="1"/>
    <field name="reasonforchange" editable="1"/>
    <field name="relatedroadareahref" editable="1"/>
    <field name="roadclassification" editable="1"/>
    <field name="roadclassificationnumber" editable="1"/>
    <field name="roadname" editable="1"/>
    <field name="roadnamelang" editable="1"/>
    <field name="roadstructure" editable="1"/>
    <field name="routehierarchy" editable="1"/>
    <field name="startgradeseparation" editable="1"/>
    <field name="startnodehref" editable="1"/>
    <field name="trunkroad" editable="1"/>
    <field name="validfrom" editable="1"/>
    <field name="wholelink" editable="1"/>
  </editable>
  <labelOnTop>
    <field labelOnTop="0" name="TOLL_P"/>
    <field labelOnTop="0" name="alternateidentifieridentifier"/>
    <field labelOnTop="0" name="alternateidentifieridentifierscheme"/>
    <field labelOnTop="0" name="alternatename"/>
    <field labelOnTop="0" name="alternatenamelang"/>
    <field labelOnTop="0" name="averagewidth"/>
    <field labelOnTop="0" name="beginlifespanversion"/>
    <field labelOnTop="0" name="confidencelevel"/>
    <field labelOnTop="0" name="cyclefacility"/>
    <field labelOnTop="0" name="direction"/>
    <field labelOnTop="0" name="directionalitytitle"/>
    <field labelOnTop="0" name="endgradeseparation"/>
    <field labelOnTop="0" name="endnodehref"/>
    <field labelOnTop="0" name="fictitious"/>
    <field labelOnTop="0" name="formofway"/>
    <field labelOnTop="0" name="formspartofhref"/>
    <field labelOnTop="0" name="formspartofrole"/>
    <field labelOnTop="0" name="gml_id"/>
    <field labelOnTop="0" name="identifier"/>
    <field labelOnTop="0" name="indirection"/>
    <field labelOnTop="0" name="inoppositedirection"/>
    <field labelOnTop="0" name="length"/>
    <field labelOnTop="0" name="localid"/>
    <field labelOnTop="0" name="matchstatus"/>
    <field labelOnTop="0" name="minimumwidth"/>
    <field labelOnTop="0" name="minmaxnumberoflanes"/>
    <field labelOnTop="0" name="namespace"/>
    <field labelOnTop="0" name="numberoflanes"/>
    <field labelOnTop="0" name="ogc_fid"/>
    <field labelOnTop="0" name="operationalstate"/>
    <field labelOnTop="0" name="primaryroute"/>
    <field labelOnTop="0" name="provenance"/>
    <field labelOnTop="0" name="reasonforchange"/>
    <field labelOnTop="0" name="relatedroadareahref"/>
    <field labelOnTop="0" name="roadclassification"/>
    <field labelOnTop="0" name="roadclassificationnumber"/>
    <field labelOnTop="0" name="roadname"/>
    <field labelOnTop="0" name="roadnamelang"/>
    <field labelOnTop="0" name="roadstructure"/>
    <field labelOnTop="0" name="routehierarchy"/>
    <field labelOnTop="0" name="startgradeseparation"/>
    <field labelOnTop="0" name="startnodehref"/>
    <field labelOnTop="0" name="trunkroad"/>
    <field labelOnTop="0" name="validfrom"/>
    <field labelOnTop="0" name="wholelink"/>
  </labelOnTop>
  <reuseLastValue>
    <field reuseLastValue="0" name="TOLL_P"/>
    <field reuseLastValue="0" name="alternateidentifieridentifier"/>
    <field reuseLastValue="0" name="alternateidentifieridentifierscheme"/>
    <field reuseLastValue="0" name="alternatename"/>
    <field reuseLastValue="0" name="alternatenamelang"/>
    <field reuseLastValue="0" name="averagewidth"/>
    <field reuseLastValue="0" name="beginlifespanversion"/>
    <field reuseLastValue="0" name="confidencelevel"/>
    <field reuseLastValue="0" name="cyclefacility"/>
    <field reuseLastValue="0" name="direction"/>
    <field reuseLastValue="0" name="directionalitytitle"/>
    <field reuseLastValue="0" name="endgradeseparation"/>
    <field reuseLastValue="0" name="endnodehref"/>
    <field reuseLastValue="0" name="fictitious"/>
    <field reuseLastValue="0" name="formofway"/>
    <field reuseLastValue="0" name="formspartofhref"/>
    <field reuseLastValue="0" name="formspartofrole"/>
    <field reuseLastValue="0" name="gml_id"/>
    <field reuseLastValue="0" name="identifier"/>
    <field reuseLastValue="0" name="indirection"/>
    <field reuseLastValue="0" name="inoppositedirection"/>
    <field reuseLastValue="0" name="length"/>
    <field reuseLastValue="0" name="localid"/>
    <field reuseLastValue="0" name="matchstatus"/>
    <field reuseLastValue="0" name="minimumwidth"/>
    <field reuseLastValue="0" name="minmaxnumberoflanes"/>
    <field reuseLastValue="0" name="namespace"/>
    <field reuseLastValue="0" name="numberoflanes"/>
    <field reuseLastValue="0" name="ogc_fid"/>
    <field reuseLastValue="0" name="operationalstate"/>
    <field reuseLastValue="0" name="primaryroute"/>
    <field reuseLastValue="0" name="provenance"/>
    <field reuseLastValue="0" name="reasonforchange"/>
    <field reuseLastValue="0" name="relatedroadareahref"/>
    <field reuseLastValue="0" name="roadclassification"/>
    <field reuseLastValue="0" name="roadclassificationnumber"/>
    <field reuseLastValue="0" name="roadname"/>
    <field reuseLastValue="0" name="roadnamelang"/>
    <field reuseLastValue="0" name="roadstructure"/>
    <field reuseLastValue="0" name="routehierarchy"/>
    <field reuseLastValue="0" name="startgradeseparation"/>
    <field reuseLastValue="0" name="startnodehref"/>
    <field reuseLastValue="0" name="trunkroad"/>
    <field reuseLastValue="0" name="validfrom"/>
    <field reuseLastValue="0" name="wholelink"/>
  </reuseLastValue>
  <dataDefinedFieldProperties/>
  <widgets/>
  <previewExpression>"namespace"</previewExpression>
  <mapTip></mapTip>
  <layerGeometryType>1</layerGeometryType>
</qgis>
